<!DOCTYPE html>
<html>
<head>
    <style>
        input[type=text] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid red;
            border-radius: 4px;
        }
    </style>
</head>
<body>

<p>Text fields with borders:</p>

<form action="{{route('categoria_tipos.update', $categoria_tipo->id)}}" method="post">
    {{ csrf_field() }}
    @method('PUT')
    <input type="text" name="nome" placeholder="Nome" value="{{strtoupper($categoria_tipo->nome)}}" required>
    <input type="text" name="descrizione" placeholder="Descrizione" value="{{$categoria_tipo->descrizione}}" >
    <button type="submit" class="btn btn-danger">Aggiorna</button>
</form>

</body>
</html>
