<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
PIPPO
{{$categoria_tipi}}
<h2>Tipi Categorie</h2>

<a href="{{route('categoria_tipos.create')}}">
    <img src="https://img.icons8.com/cute-clipart/30/000000/new.png"/>
</a>

<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Actions</th>
    </tr>
    @if (count($categoria_tipi) >= 1)
        @foreach ($categoria_tipi as $categoria_tipo)
            <tr>
                <td style="width: 10%">{{$categoria_tipo->id}}</td>
                <td style="width: 20%">{{strtoupper($categoria_tipo->nome)}}</td>
                <td style="width: 50%">{{$categoria_tipo->descrizione}}</td>
                <td>
                    <a href="{{route('categoria_tipos.edit', $categoria_tipo->id)}}">
                        <img src="https://img.icons8.com/wired/30/000000/edit.png" alt="Picture Description">
                    </a>
                    <a href="{{route('categoria_tipos.show', $categoria_tipo->id)}}">
                        <img src="https://img.icons8.com/wired/30/000000/show-property.png" alt="Picture Description">
                    </a>
                    <a href="{{route('categoria_tipos.delete', $categoria_tipo->id)}}">
                        <img src="https://img.icons8.com/wired/30/000000/delete-forever.png" alt="Picture Description">
                    </a>
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="4" style="text-align: center">No records</td>
        </tr>
    @endif

</table>

</body>
</html>
