<!DOCTYPE html>
<html>
<head>
    <style>
        input[type=text] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid red;
            border-radius: 4px;
        }
    </style>
</head>
<body>

<p>Text fields with borders:</p>

<form>
    <input type="text" name="nome" value="{{$categoria->nome}}" placeholder="Nome Categoria" required readonly>
</form>

</body>
</html>
