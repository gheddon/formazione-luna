<!DOCTYPE html>
<html>
<head>
    <style>
        input[type=text] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid red;
            border-radius: 4px;
        }
    </style>
</head>
<body>

<p>Text fields with borders:</p>

<form action="{{route('categorias.update', $categoria->id)}}" method="post">
    {{ csrf_field() }}
    @method('PUT')
    <input type="text" name="nome" placeholder="Email Address" value="{{strtoupper($categoria->nome)}}" required>
    <button type="submit" class="btn btn-danger">Aggiorna</button>
</form>

</body>
</html>
