<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
PIPPO
{{$categorie}}
<h2>Categorie</h2>

<a href="{{route('categorias.create')}}">
    <img src="https://img.icons8.com/cute-clipart/30/000000/new.png"/>
</a>

<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Actions</th>
    </tr>
    @if (count($categorie) >= 1)
        @foreach ($categorie as $categoria)
            <tr>
                <td style="width: 10%">{{$categoria->id}}</td>
                <td style="width: 70%">{{strtoupper($categoria->nome)}}</td>
                <td>
                    <a href="{{route('categorias.edit', $categoria->id)}}">
                        <img src="https://img.icons8.com/wired/30/000000/edit.png" alt="Picture Description">
                    </a>
                    <a href="{{route('categorias.show', $categoria->id)}}">
                        <img src="https://img.icons8.com/wired/30/000000/show-property.png" alt="Picture Description">
                    </a>
                    <a href="{{route('categorias.delete', $categoria->id)}}">
                        <img src="https://img.icons8.com/wired/30/000000/delete-forever.png" alt="Picture Description">
                    </a>
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="3" style="text-align: center">No records</td>
        </tr>
    @endif

</table>

</body>
</html>
