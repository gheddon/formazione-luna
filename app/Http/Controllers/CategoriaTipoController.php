<?php

namespace App\Http\Controllers;

use App\Models\CategoriaTipo;
use Illuminate\Http\Request;

class CategoriaTipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('categoria_tipos.index', ['categoria_tipi'=> CategoriaTipo::all()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categoria_tipos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipo_categoria = new CategoriaTipo($request->all());
        $tipo_categoria->save();
        return view('categoria_tipos.index', ['categoria_tipi'=> CategoriaTipo::all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CategoriaTipo  $categoriaTipo
     * @return \Illuminate\Http\Response
     */
    public function show(CategoriaTipo $categoriaTipo)
    {
        //
        return view('categoria_tipos.show', ['categoria_tipo'=> $categoriaTipo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CategoriaTipo  $categoriaTipo
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoriaTipo $categoriaTipo)
    {
        //
        return view('categoria_tipos.edit', ['categoria_tipo'=> $categoriaTipo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CategoriaTipo  $categoriaTipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoriaTipo $categoriaTipo)
    {
        //
        $categoriaTipo->fill($request->all());
        $categoriaTipo->save();
        return view('categoria_tipos.index', ['categoria_tipi'=> CategoriaTipo::all()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CategoriaTipo  $categoriaTipo
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoriaTipo $categoriaTipo)
    {
        //
        $categoriaTipo->delete();
        return view('categoria_tipos.index', ['categoria_tipi'=> CategoriaTipo::all()]);
    }
}
