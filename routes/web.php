<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\CategoriaTipoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('categoria/showView/{categoria}', [CategoriaController::class, 'showView']);
//Route::get('categoria/{categoria}/edit', [CategoriaController::class, 'edit']);
Route::resource('categorias', CategoriaController::class);
Route::get('categorias/{categoria}', [CategoriaController::class, 'destroy'])->name('categorias.delete');

Route::resource('categoria_tipos', CategoriaTipoController::class);
Route::get('categoria_tipos/{categoria_tipo}', [CategoriaTipoController::class, 'destroy'])->name('categoria_tipos.delete');
