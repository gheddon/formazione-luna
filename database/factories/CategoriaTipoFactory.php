<?php

namespace Database\Factories;

use App\Models\CategoriaTipo;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoriaTipoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CategoriaTipo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
